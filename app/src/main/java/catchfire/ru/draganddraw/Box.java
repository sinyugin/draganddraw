package catchfire.ru.draganddraw;

import android.graphics.PointF;

/**
 * Keeps coordinats of the beginning point and the ending point.
 */

public class Box {

    private PointF mOrigin, mCurrent;

    public Box(PointF origin) {
        mOrigin = origin;
        mCurrent = origin;
    }

    public void setCurrent(PointF current){
        mCurrent = current;
    }

    public PointF getOrigin(){
        return mOrigin;
    }

    public PointF getCurrent(){
        return mCurrent;
    }
}
