package catchfire.ru.draganddraw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sinyu on 26.10.2017.
 */

public class BoxDrawingView extends View{

    public static final String TAG = "BoxDrawingView";
    private Box mCurrentBox;
    private List<Box> mList = new ArrayList<>();
    private Paint mBoxPaint, mBackgroundPaint;

    //used when creating the view in code
    public BoxDrawingView(Context context) {
        this(context, null);
    }

    //used when creating the view from xml
    public BoxDrawingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(Color.RED);

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(0xfff8efe0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPaint(mBackgroundPaint);
        Log.d(TAG, "we are drawing ");
        for (Box box: mList) {
            float left = Math.min(box.getOrigin().x, box.getCurrent().x);
            float right = Math.max(box.getOrigin().x, box.getCurrent().x);
            float top = Math.min(box.getOrigin().y, box.getCurrent().y);
            float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);
        canvas.drawRect(left, top, right, bottom, mBoxPaint);
            Log.d(TAG, "we are drawing " + left + " " + right + " " + top + " " + bottom);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //String action = "";
        PointF coors = new PointF(event.getX(), event.getY());
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                //action = "action_down";
                mCurrentBox = new Box(coors);
                mList.add(mCurrentBox);
                break;
            case MotionEvent.ACTION_UP:
                //action = "action_up";
                mCurrentBox = null;
                break;
            case MotionEvent.ACTION_CANCEL:
                //action = "action_cancel";
                mCurrentBox = null;
                break;
            case MotionEvent.ACTION_MOVE:
                //action = "action_move";
                if(mCurrentBox != null){
                    mCurrentBox.setCurrent(coors);
                    //перерисовывает прямоугольник при движении пальца
                    invalidate();
                }
                break;
        }
        //Log.d(TAG, action + "   x: " + coors.x + "   y: " + coors.y);
        return true;
    }
}
